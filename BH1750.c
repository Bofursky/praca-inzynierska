/*
 * BH1750.c
 *
 *  Created on: 14.10.2017
 *      Author: Skynets
 */
#include <util/delay.h>

#include "I2C.h"
#include "BH1750.h"

void BH1750_Active(void)
{
	I2C_Start();
	I2C_SendAddr(BH1750_ADDR);
	I2C_SendByte(BH1750_POWER_ON);
	I2C_SendByte(BH1750_RESET);
	I2C_Stop();

}

void BH1750_Initalize(void)
{
	BH1750_Active();

	I2C_Start();
	I2C_SendAddr(BH1750_ADDR);
	I2C_SendByte(BH1750_MODE);
	I2C_Start();
	I2C_SendByte(BH1750_ADDR | TW_READ);
	lux1 = I2C_ReceiveData_ACK();
	lux2 = I2C_ReceiveData_NACK();
	_delay_ms(180);
	I2C_Stop();

}
int BH1750_getScoreLux(void)
{
	BH1750_Initalize();
	int lux_light;
	lux_light = lux1 + lux2;
	lux_light = lux_light * 10;
	lux_light = lux_light / 12;

	return lux_light;
}
