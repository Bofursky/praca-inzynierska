/*
 * Tinny24.h
 *
 *  Created on: 30.12.2017
 *      Author: Skynets
 */

#ifndef TINNY24_H_
#define TINNY24_H_

#define Tinny24_ADDR 0x10 //Adres miernika
#define Tinny24_STATUS 0x01
#define Tinny24_ADC0 0x10
#define Tinny24_ADC1 0x11
#define Tinny24_ADC2 0x12
#define Tinny24_ADC3 0x13
#define Tinny24_ADC4 0x14
#define Tinny24_ADC5 0x15

typedef union
{
	uint8_t	status;
	struct
	{
		uint8_t		tiny24_active:1;
	};
}TINNY24_STATUS;

typedef struct
{
	uint8_t status;
}TINNY24;


uint8_t Tinny24_Active(void);
void Tinny24_Initalize(void);
void Tinny24_getADC0(void);
void Tinny24_getADC1(void);
void Tinny24_getADC2(void);
void Tinny24_getADC3(void);
void Tinny24_getADC4(void);
void Tinny24_getADC5(void);

uint16_t Tinny24_getScoreADC0(void);
uint16_t Tinny24_getScoreADC1(void);
uint16_t Tinny24_getScoreADC2(void);
uint16_t Tinny24_getScoreADC3(void);
uint16_t Tinny24_getScoreADC4(void);
uint16_t Tinny24_getScoreADC5(void);

#endif /* TINNY24_H_ */
