
#ifndef BMP180_H_
#define BMP180_H_

#define BMP180_ADDR 0xEE        // Adres I2C
#define BMP180_CHIP_ID 0xD0		// Id Chip
#define BMP180_MODE 3           // oversampling setting (0-3)

typedef union
{
	uint8_t	status;
	struct
	{
		uint8_t		bmp180_active:1;
		uint8_t		bmp180_start:1;
	};
}BMP180_STATUS;

typedef struct
{
	uint8_t chip_id;				//id chipu - 0x55
}BMP180;

typedef struct
{
	int		AC1;
	int		AC2;
	int		AC3;
	unsigned int	AC4;
	unsigned int	AC5;
	unsigned int	AC6;
	int		B1;
	int		B2;
	int		MB;
	int		MC;
	int		MD;
}BMP180_INIT;

extern BMP180 bmp180;
extern BMP180_STATUS bmp180_status;
extern BMP180_INIT bmp180_init;

uint8_t BMP180_Active(void);			//Spradzenie dzia�ania czujnika
uint8_t BMP180_Initalize( void );       // Inicjalizacja czujnika
void BMP180_getTemperature(void);		//Pomiar temeratury
void BMP180_getPressure(void);			//Pomiar cisnienia
long BMP180_getScoreTemperature(void);	//Obliczanie temperatury
long BMP180_getScorePressure(void);

#endif /* BMP180_H_ */
