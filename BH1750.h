/*
 * BH1750.h
 *
 *  Created on: 14.10.2017
 *      Author: Skynets
 */

#ifndef BH1750_H_
#define BH1750_H_

#define BH1750_ADDR 0x46 //Adres czujnika
#define BH1750_POWER_ON 0x01 //Zalaczenie czujnika
#define BH1750_RESET 0x07 //Reset rejestru pomiaru
#define BH1750_MODE 0x21 // Wybor dokladnosci
// 0x20 Typ. 120ms. 0.5 lx
// 0x21 Typ. 120ms. 1 lx.
// 0x23 Typ. 16ms. 4 lx.

int lux1;
int lux2;

void BH1750_Active(void);
void BH1750_Initalize(void);
int BH1750_getScoreLux(void);

#endif /* BH1750_H_ */
