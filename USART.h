/*
 * mkuart.h
 *
 *  Created on: 2010-09-04
 *       Autor: Miros�aw Karda�
 */

#ifndef USART_H_
#define USART_H_


#define UART_BAUD 9600		// tu definiujemy interesuj�c� nas pr�dko��
#define __UBRR ((F_CPU+UART_BAUD*8UL) / (16UL*UART_BAUD)-1) // obliczamy UBRR dla U2X=0


#define UART_RX_BUF_SIZE 32 // definiujemy bufor o rozmiarze 32 bajt�w
// definiujemy mask� dla naszego bufora
#define UART_RX_BUF_MASK ( UART_RX_BUF_SIZE - 1)

#define UART_TX_BUF_SIZE 16 // definiujemy bufor o rozmiarze 16 bajt�w
// definiujemy mask� dla naszego bufora
#define UART_TX_BUF_MASK ( UART_TX_BUF_SIZE - 1)




// deklaracje funkcji publicznych

void USART_Init( uint16_t baud );

char uart_getc(void);
void uart_putc( char data );
void uart_puts(char *s);
void uart_putint(int value, int radix);
void uart_puts_P(const char *s);
void uart_putlong( uint32_t liczba, uint8_t radix );

#endif /* USART_H_ */
