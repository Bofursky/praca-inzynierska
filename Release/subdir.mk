################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../BH1750.c \
../BMP180.c \
../DHT.c \
../I2C.c \
../SPI.c \
../Tinny24.c \
../USART.c \
../main.c \
../nRF24L01.c 

OBJS += \
./BH1750.o \
./BMP180.o \
./DHT.o \
./I2C.o \
./SPI.o \
./Tinny24.o \
./USART.o \
./main.o \
./nRF24L01.o 

C_DEPS += \
./BH1750.d \
./BMP180.d \
./DHT.d \
./I2C.d \
./SPI.d \
./Tinny24.d \
./USART.d \
./main.d \
./nRF24L01.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


