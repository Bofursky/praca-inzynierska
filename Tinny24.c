/*
 * Tinny24.c
 *
 *  Created on: 30.12.2017
 *      Author: Skynets
 */

#include "I2C.h"
#include "Tinny24.h"

#define AREFF 1.10

TINNY24 tinny24;
TINNY24_STATUS tinny24_status;

uint16_t adc0, adc1, adc2, adc3, adc4, adc5;

uint32_t sredniaADC0, sredniaADC1 , sredniaADC2, sredniaADC3, sredniaADC4, sredniaADC5;
uint8_t czas= 4;
uint8_t czas1= 6;

uint8_t Tinny24_Active(void)
{
	I2C_Start();
	I2C_SendAddr(Tinny24_ADDR);	//Wsy�anie adresu urzadzenia
	I2C_SendByte(Tinny24_STATUS);
	I2C_Start();
	I2C_SendAddr(Tinny24_ADDR| TW_READ);
	tinny24.status = I2C_ReceiveData_NACK();
	I2C_Stop();

	if(tinny24.status == 0x06)
	{
		tinny24_status.tiny24_active = 1; //Dziala
		return 1;
	}
	else
	{

		tinny24_status.tiny24_active = 0;//Nie dziala
		return 0;
	}
}

void Tinny24_Initalize(void)
{

	Tinny24_Active();
}

void Tinny24_getADC0(void)
{
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_WRITE);
	I2C_SendByte(Tinny24_ADC0);
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_READ);
	adc0 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK());
	I2C_Stop();
}
void Tinny24_getADC1(void)
{
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_WRITE);
	I2C_SendByte(Tinny24_ADC1);
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_READ);
	adc1 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK());
	I2C_Stop();
}

void Tinny24_getADC2(void)
{
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_WRITE);
	I2C_SendByte(Tinny24_ADC2);
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_READ);
	adc2 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK());
	I2C_Stop();
}
void Tinny24_getADC3(void)
{
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_WRITE);
	I2C_SendByte(Tinny24_ADC3);
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_READ);
	adc3 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK());
	I2C_Stop();
}
void Tinny24_getADC4(void)
{
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_WRITE);
	I2C_SendByte(Tinny24_ADC4);
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_READ);
	adc4 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK());
	I2C_Stop();
}
void Tinny24_getADC5(void)
{
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_WRITE);
	I2C_SendByte(Tinny24_ADC5);
	I2C_SendStartAndSelect(Tinny24_ADDR | TW_READ);
	adc5 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK());
	I2C_Stop();
}

uint16_t Tinny24_getScoreADC0(void)
{
	uint16_t scoreADC0;
	Tinny24_getADC0();

	sredniaADC0 = sredniaADC0 * czas;
	sredniaADC0 = sredniaADC0 + adc0;
	sredniaADC0 = sredniaADC0 / (czas+1);

	scoreADC0=((sredniaADC0*AREFF/1024)*10000);
	//scoreADC0 = adc0;
	//scoreADC0 = sredniaADC0;
	return scoreADC0;
}
uint16_t Tinny24_getScoreADC1(void)
{
	uint16_t scoreADC1;
	Tinny24_getADC1();

	if(adc1 == 0)
	{
		adc1 = sredniaADC1;
	}
	sredniaADC1 = sredniaADC1 * czas1;
	sredniaADC1 = sredniaADC1 + adc1;
	sredniaADC1 = sredniaADC1 / (czas1+1);

	scoreADC1=((sredniaADC1*AREFF/1024)*10000);
	//scoreADC1= adc1;
	return scoreADC1;
}
uint16_t Tinny24_getScoreADC2(void)
{
	uint16_t scoreADC2;
	Tinny24_getADC2();
	sredniaADC2 = sredniaADC2 * czas;
	sredniaADC2 = sredniaADC2 + adc2;
	sredniaADC2 = sredniaADC2 / (czas+1);
	scoreADC2=(sredniaADC2*AREFF/1024)*4000;
	//scoreADC2= adc2;
	return scoreADC2;
}
uint16_t Tinny24_getScoreADC3(void)
{
	uint16_t scoreADC3;
	Tinny24_getADC3();
	sredniaADC3 = sredniaADC3 * czas;
	sredniaADC3 = sredniaADC3 + adc3;
	sredniaADC3 = sredniaADC3 / (czas+1);
	scoreADC3=(sredniaADC3*AREFF/1024)*4000;
	//scoreADC3= adc3;
	return scoreADC3;
}
uint16_t Tinny24_getScoreADC4(void)
{
	uint16_t scoreADC4;
	Tinny24_getADC4();
	sredniaADC4 = sredniaADC4 * czas;
	sredniaADC4 = sredniaADC4 + adc4;
	sredniaADC4 = sredniaADC4 / (czas+1);
	scoreADC4=(sredniaADC4*AREFF/1024)*6000;
	//scoreADC4= adc4;
	return scoreADC4;
}
uint16_t Tinny24_getScoreADC5(void)
{
	uint16_t scoreADC5;
	Tinny24_getADC5();
	sredniaADC5 = sredniaADC5 * czas;
	sredniaADC5 = sredniaADC5 + adc5;
	sredniaADC5 = sredniaADC5 / (czas+1);
	scoreADC5=(sredniaADC5*AREFF/1024)*1000;
	scoreADC5 = scoreADC5 - 500;
	scoreADC5 = scoreADC5 / 10;
	return scoreADC5;
}
//scoreADC0=((sredniaADC0*AREFF/1024)/10)*10000;
	//coreADC0=(sredniaADC0*AREFF/1024);
