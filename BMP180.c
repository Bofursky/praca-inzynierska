#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "I2C.h"
#include "BMP180.h"

BMP180 bmp180;
BMP180_STATUS bmp180_status;
BMP180_INIT bmp180_init;

long t, p, B5 , UT, UP;

uint8_t BMP180_Active(void)
{
	I2C_Start();
	I2C_SendAddr(BMP180_ADDR);	//Wsy�anie adresu urzadzenia
	I2C_SendByte(BMP180_CHIP_ID); //Wys�anie adresu chipu
	I2C_Start();
	I2C_SendByte(BMP180_ADDR | TW_READ);
	bmp180.chip_id = I2C_ReceiveData_NACK();
	I2C_Stop();

	if(bmp180.chip_id == 0x55)
	{
		bmp180_status.bmp180_active = 1;
		return 1;
	}
	else
	{

		bmp180_status.bmp180_active = 0;
		return 0;
	}
}
uint8_t BMP180_Initalize(void)
{
	BMP180_Active();

	if(bmp180_status.bmp180_active)
	{
		I2C_Start();
		I2C_SendAddr(BMP180_ADDR);				//Wsy�anie adresu urzadzenia
		I2C_SendByte(0xAA); 					//adres pierwszej kom�rki w pamieci eeprom
		I2C_Start();
		I2C_SendByte(BMP180_ADDR | TW_READ);	//adres z �adaniem odczytu

		bmp180_init.AC1 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.AC2 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.AC3 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.AC4 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.AC5 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.AC6 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.B1 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.B2 = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.MB = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.MC = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_ACK() );
		bmp180_init.MD = (I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK() );

		I2C_Stop();
	return 1;
	}
	else
	{
		return 0;
	}
}
void BMP180_getTemperature(void)
{
	I2C_Start();
	I2C_SendAddr(BMP180_ADDR);
	I2C_SendByte(0xF4);
	I2C_SendByte(0X2E);
	I2C_Stop();
	_delay_ms(6);
	I2C_Start();
	I2C_SendAddr(BMP180_ADDR);
	I2C_SendByte(0xF6);
	I2C_Start();
	I2C_SendByte(BMP180_ADDR | TW_READ);
	UT = ( I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK() );
	I2C_Stop();
}
void BMP180_getPressure(void)
{
	I2C_Start();
	I2C_SendAddr(BMP180_ADDR);
	I2C_SendByte(0xF4);
	I2C_SendByte(BMP180_MODE << 6 | 0x34 );
	I2C_Stop();
	_delay_ms(6 + (3 << BMP180_MODE));
	I2C_Start();
	I2C_SendAddr(BMP180_ADDR);
	I2C_SendByte(0xF6);
	I2C_Start();
	I2C_SendByte(BMP180_ADDR | TW_READ);
	UP =  ( ( (long)I2C_ReceiveData_ACK() << 16)  |  ( (long)I2C_ReceiveData_ACK() ) << 8 | ( I2C_ReceiveData_NACK() ) ) >> ( 8 - BMP180_MODE);
	I2C_Stop();

}
long BMP180_getScoreTemperature(void)
{
        long x1,x2, t;
        BMP180_getTemperature();

        x1 = ((long)UT -  bmp180_init.AC6) *  bmp180_init.AC5 >> 15;
        x2 = ((int32_t) bmp180_init.MC << 11) / (x1 +  bmp180_init.MD);
        B5 = x1 + x2;
        t = ((B5 + 8)>>4);
        return t;
}
long BMP180_getScorePressure(void)
{
//      long x1,x2,x3,b3,b6;
        int32_t x1,x2,x3,b3,b6;
        unsigned long b4,b7;
        long p;

        BMP180_getPressure();
        b6 = B5 - 4000;
        x1 = ( bmp180_init.B2* (b6 * b6) >> 12) >> 11;
        x2 = ( bmp180_init.AC2 * b6) >> 11;
        x3 = x1 + x2;
        b3 = (((((long) bmp180_init.AC1) * 4 + x3) << BMP180_MODE) + 2) >> 2;
        x1 = ( bmp180_init.AC3 * b6) >> 13;
        x2 = ( bmp180_init.B1 * ((b6 * b6) >> 12)) >> 16;
        x3 = ((x1 + x2) + 2) >> 2;
        b4 = ( bmp180_init.AC4 * (unsigned long)(x3 + 32768)) >> 15;
        b7 = ((unsigned long)UP - b3) * (50000 >> BMP180_MODE);
        p = b7 < 0x80000000 ? (b7 << 1) / b4 : (b7 / b4) << 1;
        x1 = (p >> 8) * (p >> 8);
        x1 = (x1 * 3038) >> 16;
        x2 = (-7357 * p) >> 16;
        p = p + ((x1 + x2 + 3791) >> 4);
        return p;
}
