#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "USART.h"
#include "DHT.h"
#include "I2C.h"
#include "BMP180.h"
#include "BH1750.h"
#include "Tinny24.h"
#include "SPI.h"
#include "nRF24L01.h"
#include "nRF24L01_memory_map.h"

//Zmienne stale
volatile uint8_t cnt=0;
char send = 0, timeIf = 0, timerSendToRPI= 0, timeSwitch = 1, startMeasure = 1;
int luxSumTemp = 0, tempSumTemp = 0, wilgSumTemp = 0;
int luxSum, tempSum, wilgSum;
int luxTab[10], tempTab[10], wilgTab[10];
int i = 0, timer = 0, lux, sum = 0;
int timerADC = 0, TinnyADC0, TinnyADC1, TinnyADC2, TinnyADC3, TinnyADC4, TinnyADC5;
int timerSharp = 0;
int volt, dust;
long pressSumTemp = 0, press, pressSum, pressTab[10];

float temp = 0, wilg = 0;


//Zmienne tymczasowe
uint8_t status;


void Result(void);
void SendToRpi(void);
void Miernik(void);
void Tinny24(void);
void DustMeter(void);

int main(void)
{

	_delay_ms(3000);

	USART_Init( __UBRR );			// inicjalizacja UART
	sei();// globalne odblokowanie przerwa�
	uart_puts("Inicjalizacja UART    ");
	uart_putc('\r');
	uart_putc('\n');

	nRF_init();
	nRF_RX_Power_Up();
	uart_puts("Inicjalizacja NRF     ");
	uart_putc('\r');
	uart_putc('\n');

	I2C_Initalize();
	uart_puts("Inicjalizacja I2C     ");
	uart_putc('\r');
	uart_putc('\n');

    BH1750_Initalize();
    uart_puts("Inicjalizacja BH    ");
	uart_putc('\r');
	uart_putc('\n');

	BMP180_Initalize();
	uart_puts("Inicjalizacja BMP    ");
	uart_putc('\r');
	uart_putc('\n');



   // Tinny24_Initalize();
    uart_puts("Inicjalizacja Tinny    ");
	uart_putc('\r');
	uart_putc('\n');

    uart_puts("------Koniec--------    ");
	uart_putc('\r');
	uart_putc('\n');

    nRF_RX_Power_Up();


	ADCSRA =   (1<<ADEN) //w��czenie ADC
				|(0<<ADPS0) //ADPS2:0 ustawienie preskalera na 128
	            |(1<<ADPS1)
	            |(1<<ADPS2);
	sei();

	TCCR0B |= (1<<CS02);
	TIMSK0 |= (1<<TOIE0);

	DDRB |= (1<<PB2);

    while(1)
    {

    	uart_puts("");

    	//Miernik();
    	//Tinny24();

    	if(timerADC >= 1)
		{
    		//TinnyADC0 = Tinny24_getScoreADC0();
    		//TinnyADC1 = Tinny24_getScoreADC1();
    		//TinnyADC2 = Tinny24_getScoreADC2();
    		//TinnyADC3 = Tinny24_getScoreADC3();
    		//TinnyADC4 = Tinny24_getScoreADC4();
    		//TinnyADC5 = Tinny24_getScoreADC5();
    		timerADC = 0;


    		//Debug
    		//uart_putc('\r');
    		//uart_putc('\n');
    		//uart_puts("  mA: ");
    		//uart_putint(TinnyADC1, 10);
    		//uart_putc('\r');
    		//uart_putc('\n');
    		//uart_puts("Temperatura: ");
    		//uart_putint(TinnyADC5, 10);
    	}
        //5 = 1sek
        //50 = 10sek
    	if(timer >= 5 && startMeasure == 1)// startMeasure 1
    	{

    		Result();
    		DustMeter();
    		pressTab[i]=press;
    		luxTab[i]=lux;
    		tempTab[i]=temp;
    		wilgTab[i]=wilg;

			i++;
			sum++;

	    	if(sum == 10)
	    	{
	    		pressSum = (pressTab[0]+pressTab[1]+pressTab[2]+pressTab[3]+pressTab[4]+pressTab[5]+pressTab[6]+pressTab[7]+pressTab[8]+pressTab[9])/10;
	    		luxSum = (luxTab[0]+luxTab[1]+luxTab[2]+luxTab[3]+luxTab[4]+luxTab[5]+luxTab[6]+luxTab[7]+luxTab[8]+luxTab[9])/10;
	    		tempSum = (tempTab[0]+tempTab[1]+tempTab[2]+tempTab[3]+tempTab[4]+tempTab[5]+tempTab[6]+tempTab[7]+tempTab[8]+tempTab[9])/10;
	    		wilgSum = (wilgTab[0]+wilgTab[1]+wilgTab[2]+wilgTab[3]+wilgTab[4]+wilgTab[5]+wilgTab[6]+wilgTab[7]+wilgTab[8]+wilgTab[9])/10;
	    		i = 0;//Zerowanie tablicy
	    		send = 1;//Zmienna od wysylania danych do RPI
	    		timeIf = 1;//Wlaczenie timera dla switcza
	            for( int i = 0; i <= 9; i++)
	            {
	            	pressTab[i] = 0;
	            	luxTab[i] = 0;
	            	tempTab[i] = 0;
	            	wilgTab[i] = 0;
	            }
	            sum = 0;//Zerowanie petli if
	    	}
	    	timer = 0;//Zerowanie timera
    	}

    	if(send == 1)
    	{
    		startMeasure = 0;//Zmienna oczekujaca na koniec wysylania do RPI
    		SendToRpi();
    	}
    	nRF_RX_EVENT();
    }

}
void DustMeter(void)
{
	PORTB &= ~(1<<PB2);//Stan niski
	_delay_us(280);

	ADMUX  = (1<<REFS0)|(1<<REFS1)|(0<<MUX1)|(0<<MUX0);
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));

	_delay_us(40);
	PORTB  |= (1<<PB2);//Wysoki
	_delay_us(9680);

	if (ADC >= 240)
	{
		volt = ADC * (1.1 / 1024)*1000;
		dust = (0.17 * (ADC * (1.1 / 1024)) - 0.1)*1000;
	//	uart_putc('\r');
	//	uart_putc('\n');
	//	uart_puts("ADC: ");
	//	uart_putint(ADC, 10);
    //	uart_puts("  V: ");
    //	uart_putint(volt, 10);
    //	uart_puts(" Pyl[ug/m3]: ");
    //	uart_putint(dust, 10);

	}
	else
	{
		dust = 0;
	}
}

void Result(void)
{
	dht_gettemperaturehumidity(&temp, &wilg);
	press = BMP180_getScorePressure();
    lux = BH1750_getScoreLux();
}

void SendToRpi(void)
{
	char bufor[32];

	pressSumTemp = pressSum;
	luxSumTemp = luxSum;
	tempSumTemp = tempSum;
	wilgSumTemp = wilgSum;

	if(timerSendToRPI >= 25)//40Sekund wysylania
	{

		switch (timeSwitch)
		{
			case 1:
				sprintf(bufor,"*-*T>%d<T-*-",tempSumTemp);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano temperature ");
				uart_putint(tempSumTemp, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 2:
				sprintf(bufor,"*-*L>%d<L-*-",luxSumTemp);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano swiatlo ");
				uart_putint(luxSumTemp, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 3:
				pressSumTemp = pressSumTemp/90;
				sprintf(bufor,"*-*C>%ld<C-*-",pressSumTemp);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano cisnienie ");
				uart_putint(pressSumTemp, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 5:
				sprintf(bufor,"*-*W>%d<W-*-",wilgSumTemp);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano wilgotnosc ");
				uart_putint(wilgSumTemp, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 6:
				sprintf(bufor,"*-*mA>%d<mA-*-",TinnyADC1);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano mA ");
				uart_putint(TinnyADC1, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 7:
				sprintf(bufor,"*-*3v3>%d<3v3-*-",TinnyADC2);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano 3v3 ");
				uart_putint(TinnyADC2, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 8:
				sprintf(bufor,"*-*4.2>%d<4.2-*-",TinnyADC3);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano 4.2V ");
				uart_putint(TinnyADC3, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 9:
				sprintf(bufor,"*-*So>%d<So-*-",TinnyADC4);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano Solar V ");
				uart_putint(TinnyADC4, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 10:
				sprintf(bufor,"*-*mAS>%d<mAS-*-",TinnyADC0);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano mA Solar ");
				uart_putint(TinnyADC0, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 11:
				sprintf(bufor,"*-*TB>%d<TB-*-",TinnyADC5);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano T-Batt ");
				uart_putint(TinnyADC5, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;
				break;
			case 12:
				sprintf(bufor,"*-*D>%d<D-*-",dust);
				nRF_SendDataToAir((char *)bufor);
				uart_puts("Wyslano Dust ");
				uart_putint(dust, 10);
				uart_putc('\r');
				uart_putc('\n');
				timerSendToRPI = 0;

				//////////////////////
				send = 0;//Ustawic zmienne w ostanim wysyle
				timeIf = 0;
				timeSwitch = 0;
				startMeasure = 1;
				//////////////////
				break;
		}
		timeSwitch++;
	}
}
//Debug
void Tinny24(void)
{
	uart_putc('\r');
	uart_putc('\n');
	uart_puts("mA Solar: ");
	uart_putint(TinnyADC0, 10);
	uart_putc('\r');
	uart_putc('\n');
	uart_puts("mA Stabilizator: ");
	uart_putint(TinnyADC1, 10);
	uart_putc('\r');
	uart_putc('\n');
	uart_puts("V Stabilizator: ");
	uart_putint(TinnyADC2, 10);
	uart_putc('\r');
	uart_putc('\n');
	uart_puts("V Bateria: ");
	uart_putint(TinnyADC3, 10);
	uart_putc('\r');
	uart_putc('\n');
	uart_puts("V Solar: ");
	uart_putint(TinnyADC4, 10);
	uart_putc('\r');
	uart_putc('\n');
	uart_puts("Temp: ");
	uart_putint(TinnyADC5, 10);
	uart_putc('\r');
	uart_putc('\n');

}
uint8_t i2c_status(void) {
    uint8_t status;
    status = (TWSR & 0xF8);
    return status;
}

void Miernik(void)
{
	I2C_Start();
	I2C_SendAddr(Tinny24_ADDR);//HEX
	status = i2c_status();
	I2C_Stop();
	uart_puts("   ");
	uart_putint(status, 16);
	_delay_ms(1000);
}


// Timer co 200ms
ISR(TIMER0_OVF_vect)
{
	TCNT0 = 6;
	cnt++;
	if(cnt>24)
	{
		if(timeIf == 1)
		{
			timerSendToRPI++;
		}
		timerSharp++;
		timerADC++;
		timer++;
		cnt=0;
	}

}
